package mono.android.app;

public class XamarinAndroidEnvironmentVariables
{
	// Variables are specified the in "name", "value" pairs
	public static final String[] Variables = new String[] {
		"MONO_LOG_LEVEL", "info",
		"XAMARIN_BUILD_ID", "52402d99-b3c5-4e6b-bf1f-d99f187c53ea",
		"XA_HTTP_CLIENT_HANDLER_TYPE", "Xamarin.Android.Net.AndroidClientHandler",
		"XA_TLS_PROVIDER", "btls",
		"MONO_GC_PARAMS", "major=marksweep-conc",

	};
}
